# web-v1

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Config nginx
'
server {
    listen       8300 default_server;
    listen       [::]:8300 default_server;
    server_name  _;
    root         /usr/share/nginx/html/dist;
    #root         /usr/share/nginx/html/500;
    # Load configuration files for the default server block.
    include /etc/nginx/default.d/*.conf;
    #include snippets/phpMyAdmin.conf;
    location / {
        #try_files $uri$args $uri$args/ /index.html
         try_files $uri /index.html;
        #expires       0;
        #add_header    Cache-Control  public;
        #add_header    Cache-Control  no-store;
        #add_header    Cache-Control  no-cache;
    }
    error_page 404 /404.html;
        location = /40x.html {
    }

    error_page 500 502 503 504 /50x.html;
        location = /50x.html {
    }
}
'
