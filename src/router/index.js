import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Comingsoon from '../views/Comingsoon.vue';
import Dashboard from '../views/Dashboard.vue';
import Blog from '../views/Blog.vue';
import How from '../views/Howitwork.vue';
import Contact from '../views/Contactus.vue';
import Services from '../views/Services.vue';
import Pagenotfound from '../views/Pagenotfound.vue';
Vue.use(VueRouter);
const routes = [
  {
    path: '/', component: Dashboard,
    children: [
      { path: '/', redirect:'/home'},
      {
        path: '/home',
        name: 'home',
        component: Home
      },
      {
        path: '/blog',
        name: 'blog',
        component: Blog
      },
      {
        path: '/how',
        name: 'how',
        component: How
      },
      {
        path: '/contact',
        name: 'contact',
        component: Contact
      },
      {
        path: '/services',
        name: 'services',
        component: Services
      },
    ]
  },
    {
    path: '/comingsoon',
    name: 'comingsoon',
    component: Comingsoon
  },
  {
    path: "/404",
    name: '404',
    component: Pagenotfound
  },
  {
    path: "**",
    redirect:'/404'
  },
]

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
